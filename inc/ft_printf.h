/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <ahammou-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 09:14:08 by ahammou-          #+#    #+#             */
/*   Updated: 2019/03/25 16:50:46 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <stdio.h>

typedef struct	s_printf
{
	char		*fmt;
	char 		*fmt_cpy;
	char 		*fmt_treat;
	size_t	i;
	int			len;
	va_list	ap;
	char		flag[6];
	int			width;
	int			precision;
	char		length[2];
	char		specifier;
	char		*specifier_opt;
	char		*flag_opt;
	char		*length_opt;
}								t_printf;

/*
** ---------------------------- FT_PRINTF_FUNCTIONS --------------------------/
*/
int				ft_printf(const char *format, ...);
int				ft_parse(t_printf *p);
int				ft_get_opt(t_printf *p);
t_printf	*ft_initialize(t_printf *p);
t_printf	*ft_initialize_opt(t_printf *p);
t_printf	*ft_print_specifier(t_printf *p);

/*
** ---------------------------- FT_GET_FUNCTIONS ----------------------------/
*/
t_printf		*ft_get_flag(t_printf *p);
t_printf		*ft_get_width(t_printf *p);
t_printf		*ft_get_precision(t_printf *p);
t_printf		*ft_get_length(t_printf *p);
t_printf		*ft_get_specifier(t_printf *p);

/*
** -------------------- FT_CONVERT & FT_PRINT_FUNCTIONS ----------------------/
*/
int					ft_convert_length(t_printf *p);
t_printf		*ft_print_spec_di(t_printf *p);
t_printf		*ft_print_spec_o(t_printf *p);
t_printf		*ft_print_spec_u(t_printf *p);
t_printf		*ft_print_spec_x(t_printf *p);
t_printf		*ft_print_spec_c(t_printf *p);
t_printf		*ft_print_spec_s(t_printf *p);
t_printf		*ft_print_spec_p(t_printf *p);
t_printf		*ft_print_spec_percent(t_printf *p);
#endif
