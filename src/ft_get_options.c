/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <ahammou-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/24 15:15:06 by ahammou-          #+#    #+#             */
/*   Updated: 2019/03/25 16:51:26 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"
#include "../libft/libft.h"

/*
** ------------ %[FLAG] [WIDTH] [.PRECISION][LENGTH] [SPECIFIER] -------------/
*/
t_printf  *ft_get_specifier(t_printf *p)
{
  //printf("5 - et moi je suis ft_get_specifier\n");
  size_t  i;

  i = 0;
  while (p->specifier_opt[i] != '\0')
  {
    if (p->specifier_opt[i] == p->fmt_treat[p->i])
      p->specifier = p->fmt_treat[p->i];
    i++;
  }
  return (p);
}

/*
** ft_get_length store length from p->fmt_treat in p->length
*/
t_printf *ft_get_length(t_printf *p)
{
  size_t  i;
  size_t  j;

  i = 0;
  j = 0;
  while (p->length_opt[i] != '\0')
  {
    while (p->length_opt[i] == p->fmt_treat[p->i])
    {
      p->length[j] = p->fmt_treat[p->i];
      p->length[j + 1] = '\0';
      p->i++;
      j++;
    }
    i++;
  }
  return (p);
}

/*
** ft_get_precision initialize p->precision to 0 (default value) when
** p->fmt_treat meets '.' then if digits after it save them in p->precision
*/
t_printf *ft_get_precision(t_printf *p)
{
  while (p->fmt_treat[p->i] == '.')
  {
    p->i++;
    p->precision = 0;
  }
  while (p->fmt_treat[p->i] >= '0' && p->fmt_treat[p->i] <= '9')
  {
    p->precision *= 10;
    p->precision += (p->fmt_treat[p->i] + 48);
    p->i++;
  }
  return (p);
}

/*
** Ft_get_width go through the p->fmt_treat and convert numerical char
** into digits and save the value into int var (ft_atoi like function)
*/
t_printf  *ft_get_width(t_printf *p)
{
  while (p->fmt_treat[p->i] >= '0' && p->fmt_treat[p->i] <= '9')
  {
    p->width *= 10;
    p->width += (p->fmt_treat[p->i] + 48);
    p->i++;
  }
  return (p);
}

/*
** Ft_get_flag will loop through p->flag_opt and and trough p->fmt_treat
** until each flag is saved in the appropriate var.
** It allows us to handle case like %##--0-##++0
*/
t_printf  *ft_get_flag(t_printf *p)
{
  size_t i;

  i = 0;
  while (p->flag_opt[i] != '\0')
  {
    while (p->flag_opt[i] == p->fmt_treat[p->i])
    {
      while (p->fmt_treat[p->i] == '+' && p->i++)
        p->flag[0] = '+';
      while (p->fmt_treat[p->i] == '-' && p->i++)
        p->flag[1] = '-';
      while (p->fmt_treat[p->i] == ' ' && p->i++)
        p->flag[2] = ' ';
      while (p->fmt_treat[p->i] == '#' && p->i++)
        p->flag[3] = '#';
      while (p->fmt_treat[p->i] == '0' && p->i++)
        p->flag[4] = '0';
      i = 0;
    }
    i++;
  }
  return (p);
}
