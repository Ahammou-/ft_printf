/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <ahammou-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/24 15:15:06 by ahammou-          #+#    #+#             */
/*   Updated: 2019/03/25 16:51:26 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"
#include "../libft/libft.h"

/*
** Initialize all variables and strings in structure
** even options we'll handle (instead of define spec "diouxXcsp")
*/
t_printf  *ft_initialize(t_printf *p)
{
  //printf("1 - et moi ft_initialize\n");
  p->len = 0;
  p->i = 0;
  p->fmt_cpy  = p->fmt;
  p->fmt_treat = p->fmt;
  p->flag_opt = "+- #0";
  p->length_opt = "lh";
  p->specifier_opt = "diouxXcspf%";
  return (p);
}

/*
** Set all char array and char that will set the value of option
** to '\0' (flags, length, specifier)
*/
t_printf  *ft_initialize_opt(t_printf *p)
{
  //printf("3 - moi ft_initialize_opt\n");
  p->flag[0] = '\0';
  p->flag[1] = '\0';
  p->flag[2] = '\0';
  p->flag[3] = '\0';
  p->flag[4] = '\0';
  p->flag[5] = '\0';
  p->width = 0;
  p->precision = -1;
  p->length[0] = '\0';
  p->length[1] = '\0';
  p->specifier = '\0';
  return (p);
}
