/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <ahammou-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/24 15:15:06 by ahammou-          #+#    #+#             */
/*   Updated: 2019/03/25 16:51:26 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"
#include "../libft/libft.h"

t_printf  *ft_print_spec_di(t_printf *p)
{
  int nbr;

  if (p->specifier == 'd' || p->specifier == 'i')
    nbr = va_arg(p->ap, int);
  ft_putnbr(nbr);
  return (p);
}
