/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <ahammou-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/24 15:15:06 by ahammou-          #+#    #+#             */
/*   Updated: 2019/03/25 16:51:26 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_printf.h"
#include "../libft/libft.h"

/*
** ------------ %[FLAG] [WIDTH] [.PRECISION][LENGTH] [SPECIFIER] -------------/
*/

t_printf	*ft_print_specifier(t_printf *p)
{
	char	*spec;

	//printf("6 - et moi ft_print_specifier j'imprime specifier\n");
	spec = &p->specifier;
  if (*spec == 'd' || *spec == 'i')
		ft_print_spec_di(p);
	/*else if (*spec == 'o')
		ft_print_spec_o(p);
	else if (*spec == 'u')
		ft_print_spec_u(p);
	else if (*spec == 'x' || *spec == 'X')
		ft_print_spec_x(p);
	else if (*spec == 'c')
		ft_print_spec_c(p);
	else if (*spec == 's')
		ft_print_spec_s(p);
	else if (*spec == 'p')
		ft_print_spec_p(p);
	else
		ft_print_spec_percent(p);*/
	return (p);
}

/*
** ft_get_opt is responsible of getting the options and save them
** in the appropriate struct var or strings.
*/
int		ft_get_opt(t_printf *p)
{
	//printf("4 - je suis get_options\n");
	p->i++;
	ft_get_flag(p);
	ft_get_width(p);
	ft_get_precision(p);
	ft_get_length(p);
	ft_get_specifier(p);
	ft_print_specifier(p);
	return (p->len);
}

/*
** Ft_parse will analyse and go through *fmt_cpy to get the options,
** handle them and print them
*/
int		ft_parse(t_printf *p)
{
	//printf("2 - je suis ft_parse\n");
	//if (ft_strcmp(p->fmt_cpy, "%"))
		//return (0);
	while (p->fmt_cpy[p->i] != '\0')
	{
		if (p->fmt_cpy[p->i] == '%')
		{
			ft_initialize_opt(p);
			ft_get_opt(p);
		}
		else
		{
			ft_putchar(p->fmt_cpy[p->i]);
			p->len++;
		}
		p->i++;
	}
	return (p->len);
}

int		ft_printf(const char *format, ...)
{
	t_printf	*p;

	if (!(p = (t_printf *)malloc(sizeof(t_printf))))
		return (-1);
	p->fmt = (char*)format;
	p = ft_initialize(p);
	if (format)
	{
		va_start(p->ap, format);
		p->len = ft_parse(p);
		va_end(p->ap);
	}
	free(p);
	return (p->len);
}
