/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahammou- <ahammou-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 09:40:33 by ahammou-          #+#    #+#             */
/*   Updated: 2019/03/25 16:26:22 by ahammou-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "inc/ft_printf.h"
#include "libft/libft.h"

int	main(void)
{
	int	age;
	int	gosses;

	age = 30;
	gosses = 2;
	printf("/***************************** TEST **************************/\n");

	//printf("printf : j'ai %d ans et j'ai %d enfants\n", age, gosses);
	//ft_printf("ft_printf : j'ai %d ans et j'ai %d enfants\n", age, gosses);
	printf("printf -> age %d ans\n", age);
	ft_printf("ft_printf -> age %d ans\n", age);
	return (0);
}
